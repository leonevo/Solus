#!/bin/bash
set -e
##################################################################################################################

## Add 3rd Party Repo ##

# Adding the repo
sudo eopkg add-repo leonevo https://gitlab.com/leonevo/solus-3rd-party-repo/raw/master/packages/eopkg-index.xml.xz

# Enabling the Repository
sudo eopkg enable-repo leonevo

# Removing the Repository
# sudo eopkg remove-repo leonevo

# Disabling the Repository
# sudo eopkg disable-repo leonevo

## Add SolusUnstable Repo ##

# Adding the repo
sudo eopkg ar SolusUnstable https://cdn.getsol.us/repo/unstable/eopkg-index.xml.xz

# Enabling the Repository
sudo eopkg enable-repo SolusUnstable

# Removing the Repository
# sudo eopkg remove-repo SolusUnstable

# Disabling the Repository
# sudo eopkg disable-repo SolusUnstable

## Add MegaSync Repo ##

# Adding the repo
#sudo eopkg add-repo megasync https://gitlab.com/abdulocracy/solus-megasync-repository/raw/master/packages/eopkg-index.xml.xz

# Enabling the Repository
#sudo eopkg enable-repo megasync

# Removing the Repository
# sudo eopkg remove-repo megasync

# Disabling the Repository
# sudo eopkg disable-repo megasync

## Add Theca Repo ##

# Adding the repo
# sudo eopkg add-repo Theca https://solus.davidepucci.it/eopkg-index.xml.xz

# Enabling the Repository
# sudo eopkg enable-repo Theca

# Removing the Repository
# sudo eopkg remove-repo Theca

# Disabling the Repository
# sudo eopkg disable-repo Theca

## Add Theca Unstable Repo ##

# Adding the repo
# sudo eopkg add-repo Unstable https://unstable.solus.davidepucci.it/eopkg-index.xml.xz

# Enabling the Repository
# sudo eopkg enable-repo Unstable

# Removing the Repository
# sudo eopkg remove-repo Unstable

# Disabling the Repository
# sudo eopkg disable-repo Unstable

## Add Cantalupo Repo ##

# Adding the repo
sudo eopkg add-repo Cantalupo https://github.com/cantalupo555/repo-solus/raw/master/eopkg-index.xml.xz

# Enabling the Repository
sudo eopkg enable-repo Cantalupo

# Removing the Repository
# sudo eopkg remove-repo Cantalupo

# Disabling the Repository
# sudo eopkg disable-repo Cantalupo

# Update
sudo eopkg -y up
