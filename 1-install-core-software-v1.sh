#!/bin/bash
set -e
##################################################################################################################
#
#   DO NOT JUST RUN THIS. EXAMINE AND JUDGE. RUN AT YOUR OWN RISK.
#
##################################################################################################################

# software from 'normal' repositories
sudo eopkg remove -y hexchat gedit

sudo eopkg install -y etcher plank simple-scan android-tools
sudo eopkg install -y cantarell-fonts font-awesome-ttf font-ubuntu-ttf powerline powerline-fonts
sudo eopkg install -y gcolor3 gconf gimp glances gnome-disk-utility geany geany-plugins geany-themes
sudo eopkg install -y gparted gradio nautilus-share neofetch noto-sans-ttf peek screenfetch scrot
sudo eopkg install -y simplescreenrecorder source-code-pro telegram vlc xkill 
sudo eopkg install -y sassc xclip hplip
sudo eopkg install -y conky conky-manager
sudo eopkg install -y gtkhash

# Install for a 3rd-party-repos
sudo eopkg install -y budgie-extra Webpin
#sudo eopkg install -y megasync nautilus-megasync nextcloud-client-devel

# themes & icons
sudo eopkg install -y arc-gtk-theme arc-icon-theme 
sudo eopkg install -y numix-icon-theme-circle numix-gtk-theme
sudo eopkg install -y oranchelo-icon-theme
sudo eopkg install -y papirus-icon-theme
sudo eopkg install -y plata-theme
sudo eopkg install -y materia-gtk-theme
sudo eopkg install -y la-capitaine-icon-theme

# hide plank icon
gsettings set net.launchpad.plank.dock.settings:/net/launchpad/plank/docks/dock1/ show-dock-item false

###############################################################################################

# zip/unzip
sudo eopkg install -y p7zip cabextract

# wifi
# sudo eopkg install -y broadcom-sta-common broadcom-sta-current

###############################################################################################
# Para terminar la instalación de los drivers de broadcom, ejecutar esto en un terminal paso por paso.
# sudo -i
# echo 'install wl /sbin/modprobe cfg80211; /sbin/insmod /lib/modules/$(/bin/uname -r)/kernel/drivers/net/wireless/wl.ko' > /etc/modprobe.d/wl.conf 
# echo wl > /etc/modules-load.d/wl.conf 
# modprobe wl
# exit
###############################################################################################

# Crear archivo nuevo en plantillas
touch ~/Plantillas/Documento\ vacío

echo "################################################################"
echo "###################    core software installed  ################"
echo "################################################################"

