#!/bin/bash
set -e

# Start samba

sudo systemctl start smb
sudo systemctl enable --now smb

# Add user

sudo smbpasswd -a leonevo
sudo smbpasswd -e leonevo

# Add samb.comf to /etc/samba/

sudo eopkg it -y wget
sudo wget https://gitlab.com/leonevo/samba_solus/raw/master/smb.conf -O /etc/samba/smb.conf

# Finish

sudo systemctl start smb.service
sudo systemctl start nmb.service
sudo systemctl enable smb.service
sudo systemctl enable nmb.service

echo "################################################################"
echo "###################    reboot to take effect    ################"
echo "################################################################"
