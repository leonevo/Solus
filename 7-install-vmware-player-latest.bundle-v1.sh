#!/bin/bash
#
##################################################################################################################
#
#   DO NOT JUST RUN THIS. EXAMINE AND JUDGE. RUN AT YOUR OWN RISK.
#
##################################################################################################################

# to get rid of the errors follow these steps

rm /tmp/vmware-player.bundle

wget https://www.vmware.com/go/getplayer-linux -O /tmp/vmware-player.bundle

chmod +x /tmp/vmware-player.bundle

sudo /tmp/vmware-player.bundle

rm /tmp/vmware-player.bundle

echo "start vmware player"

##################################################################################################################

echo "################################################################"
echo "###################    T H E   E N D      ######################"
echo "################################################################"


