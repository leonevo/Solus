#!/bin/bash
set -e
##################################################################################################################
#
#   DO NOT JUST RUN THIS. EXAMINE AND JUDGE. RUN AT YOUR OWN RISK.
#
##################################################################################################################

#software from 'normal' repositories

sudo eopkg remove -y konversation
sudo eopkg install -y falkon
sudo eopkg install -y etcher simple-scan
sudo eopkg install -y font-awesome-ttf font-ubuntu-ttf cantarell-fonts noto-sans-ttf
sudo eopkg install -y gimp glances geany geany-plugins geany-themes kate
sudo eopkg install -y neofetch screenfetch scrot
sudo eopkg install -y simplescreenrecorder source-code-pro telegram vlc vscode xkill 
sudo eopkg install -y conky conky-manager
sudo eopkg install -y dolphin-plugins dolphin-devel

# Install for a 3rd-party-repos

#sudo eopkg install -y budgie-extra
#sudo eopkg install -y megasync nautilus-megasync nextcloud-client-devel

# themes & icons

#sudo eopkg install -y arc-kde
#sudo eopkg install -y adapta-kde
#sudo eopkg install -y vertex-gtk-theme
#sudo eopkg install -y arc-gtk-theme arc-icon-theme 
#sudo eopkg install -y numix-icon-theme-circle numix-gtk-theme
#sudo eopkg install -y paper-icon-theme paper-gtk-theme
#sudo eopkg install -y oranchelo-icon-theme
#sudo eopkg install -y evopop-gtk-theme evopop-icon-theme
#sudo eopkg install -y papirus-icon-theme
#sudo eopkg install -y plata-theme
#sudo eopkg install -y materia-kde
#sudo eopkg install -y la-capitanie-icon-theme


###############################################################################################

# zip/unzip

sudo eopkg install -y p7zip cabextract

# wifi

# sudo eopkg install -y broadcom-sta-common broadcom-sta-current

###############################################################################################
# Para terminar la instalación de los drivers de broadcom, ejecutar esto en un terminal paso por paso.
# sudo -i
# echo 'install wl /sbin/modprobe cfg80211; /sbin/insmod /lib/modules/$(/bin/uname -r)/kernel/drivers/net/wireless/wl.ko' > /etc/modprobe.d/wl.conf 
# echo wl > /etc/modules-load.d/wl.conf 
# modprobe wl
# exit
###############################################################################################

echo "################################################################"
echo "###################    core software installed  ################"
echo "################################################################"

