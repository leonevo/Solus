#!/bin/bash
#
##################################################################################################################
#
#   DO NOT JUST RUN THIS. EXAMINE AND JUDGE. RUN AT YOUR OWN RISK.
#
##################################################################################################################


sudo find /usr/share/icons/hicolor/ -name simplescreenrecorder.png -type f -delete
sudo find /usr/share/icons/hicolor/ -name simplescreenrecorder.svg -type f -delete
sudo find /usr/share/icons/hicolor/ -name simplescreenrecorder-*.png -type f -delete
sudo find /usr/share/icons/hicolor/ -name simplescreenrecorder-*.svg -type f -delete


echo "################################################################"
echo "###################    T H E   E N D      ######################"
echo "################################################################"
